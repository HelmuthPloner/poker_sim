#include "poker.h"

#include <boost/test/unit_test.hpp>

namespace
{
    struct Fixture
    {
        Hand hand;
    };
}

BOOST_FIXTURE_TEST_SUITE(hand_test_suite, Fixture)

BOOST_AUTO_TEST_CASE(TestHistRank1)
{
    hand.setCards(
        { Suit::clubs, Rank::Jack },
        { Suit::clubs, Rank::Jack },
        { Suit::clubs, Rank::Jack },
        { Suit::clubs, Rank::Jack },
        { Suit::clubs, Rank::Jack },
        { Suit::clubs, Rank::Jack },
        { Suit::clubs, Rank::Jack });

    const auto hist_rank = hand.getRankHist();

    BOOST_REQUIRE_EQUAL(13u, hist_rank.size());

    Hist expected;
    expected.fill(0u);

    expected[static_cast<int>(Rank::Jack)] = 7u;

    BOOST_CHECK(expected == hist_rank);
}

BOOST_AUTO_TEST_CASE(TestHistRank2)
{
    hand.setCards(
        {Suit::clubs, Rank::Ace},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_10},
        {Suit::clubs, Rank::r_9},
        {Suit::clubs, Rank::r_8});

    Hist expected;
    expected.fill(0u);

    expected[static_cast<int>(Rank::Ace)] = 1u;
    expected[static_cast<int>(Rank::King)] = 1u;
    expected[static_cast<int>(Rank::Queen)] = 1u;
    expected[static_cast<int>(Rank::Jack)] = 1u;
    expected[static_cast<int>(Rank::r_10)] = 1u;
    expected[static_cast<int>(Rank::r_9)] = 1u;
    expected[static_cast<int>(Rank::r_8)] = 1u;

    const auto actual = hand.getRankHist();

    BOOST_CHECK_EQUAL_COLLECTIONS(
                std::cbegin(expected), std::cend(expected),
                std::cbegin(actual), std::cend(actual));
}

/*
 * ROYAL FLUSH
 */
BOOST_AUTO_TEST_CASE(TestIsRoyalFlushClubs)
{
    hand.setCards(
        {Suit::diamonds, Rank::r_9},
        {Suit::diamonds, Rank::r_8},
        {Suit::clubs, Rank::Ace},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_10}
                );

    BOOST_CHECK(hand.isRoyalFlush());
}

BOOST_AUTO_TEST_CASE(TestIsRoyalFlushHearts)
{
    hand.setCards(
        {Suit::spades, Rank::r_9},
        {Suit::spades, Rank::r_8},
        {Suit::hearts, Rank::Ace},
        {Suit::hearts, Rank::King},
        {Suit::hearts, Rank::Queen},
        {Suit::hearts, Rank::Jack},
        {Suit::hearts, Rank::r_10}
                );

    BOOST_CHECK(hand.isRoyalFlush());
}

BOOST_AUTO_TEST_CASE(TestIsRoyalFlushNotSorted)
{
    hand.setCards(
        {Suit::diamonds, Rank::r_9},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::Ace},
        {Suit::clubs, Rank::r_10},
        {Suit::hearts, Rank::r_8});

    BOOST_CHECK(hand.isRoyalFlush());
}

BOOST_AUTO_TEST_CASE(TestIsRoyalFlushLastHasWrongSuit)
{
    hand.setCards(
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::Ace},
        {Suit::diamonds, Rank::r_10},
        {Suit::spades, Rank::r_10},
        {Suit::hearts, Rank::r_10});

    BOOST_CHECK(!hand.isRoyalFlush());
}

BOOST_AUTO_TEST_CASE(TestIsRoyalFlush10Missing)
{
    hand.setCards(
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_9},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::r_8},
        {Suit::clubs, Rank::Ace},
        {Suit::clubs, Rank::r_7});

    BOOST_CHECK(!hand.isRoyalFlush());
}

BOOST_AUTO_TEST_CASE(TestIsRoyalFlushAceMissing)
{
    hand.setCards(
        {Suit::hearts, Rank::r_8},
        {Suit::hearts, Rank::King},
        {Suit::hearts, Rank::r_9},
        {Suit::hearts, Rank::Queen},
        {Suit::hearts, Rank::Jack},
        {Suit::hearts, Rank::r_10},
        {Suit::hearts, Rank::r_7});

    BOOST_CHECK(!hand.isRoyalFlush());
}

BOOST_AUTO_TEST_CASE(TestIsRoyalFlushWrongColor)
{
    hand.setCards(
        {Suit::hearts, Rank::King},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::r_10},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_9},
        {Suit::clubs, Rank::Ace},
        {Suit::clubs, Rank::r_8});

    BOOST_CHECK(!hand.isRoyalFlush());
}

/*
 * STRAIGHT FLUSH
 */
BOOST_AUTO_TEST_CASE(TestIsStraightFlushClubs)
{
    hand.setCards(
        {Suit::diamonds, Rank::r_9},
        {Suit::diamonds, Rank::r_8},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_10},
        {Suit::clubs, Rank::r_9}
                );

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightFlushHearts)
{
    hand.setCards(
        {Suit::spades, Rank::r_9},
        {Suit::spades, Rank::r_8},
        {Suit::hearts, Rank::King},
        {Suit::hearts, Rank::Queen},
        {Suit::hearts, Rank::Jack},
        {Suit::hearts, Rank::r_10},
        {Suit::hearts, Rank::r_9}
                );

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightFlushNotSorted)
{
    hand.setCards(
        {Suit::diamonds, Rank::r_9},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_9},
        {Suit::clubs, Rank::r_10},
        {Suit::hearts, Rank::r_8});

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightFlushLastHasWrongSuit)
{
    hand.setCards(
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::King},
        {Suit::clubs, Rank::Jack},
        {Suit::clubs, Rank::r_10},
        {Suit::diamonds, Rank::r_9},
        {Suit::spades, Rank::r_9},
        {Suit::hearts, Rank::r_9});

    BOOST_CHECK(!hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightFlushDownTo2)
{
    hand.setCards(
        {Suit::clubs, Rank::r_5},
        {Suit::clubs, Rank::r_4},
        {Suit::clubs, Rank::r_9},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::r_3},
        {Suit::clubs, Rank::r_2},
        {Suit::clubs, Rank::r_6});

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightFlushOnlyFourInARow)
{
    hand.setCards(
        {Suit::hearts, Rank::r_8},
        {Suit::hearts, Rank::King},
        {Suit::hearts, Rank::r_9},
        {Suit::hearts, Rank::Queen},
        {Suit::hearts, Rank::Ace},
        {Suit::hearts, Rank::r_10},
        {Suit::hearts, Rank::r_7});

    BOOST_CHECK(!hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightFlushWrongColor)
{
    hand.setCards(
        {Suit::clubs, Rank::r_5},
        {Suit::clubs, Rank::r_4},
        {Suit::clubs, Rank::r_9},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::r_3},
        {Suit::spades, Rank::r_2},
        {Suit::clubs, Rank::r_6});

    BOOST_CHECK(!hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightSteelWheelClubs)
{
    hand.setCards(
        {Suit::clubs, Rank::r_5},
        {Suit::clubs, Rank::r_4},
        {Suit::clubs, Rank::r_2},
        {Suit::clubs, Rank::Queen},
        {Suit::clubs, Rank::r_3},
        {Suit::spades, Rank::r_6},
        {Suit::clubs, Rank::Ace});

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightSteelWheelDiamonds)
{
    hand.setCards(
        {Suit::diamonds, Rank::Ace},
        {Suit::diamonds, Rank::r_5},
        {Suit::diamonds, Rank::r_4},
        {Suit::diamonds, Rank::r_2},
        {Suit::diamonds, Rank::r_3},
        {Suit::hearts, Rank::Queen},
        {Suit::spades, Rank::r_6}
                );

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightSteelWheelHearts)
{
    hand.setCards(
        {Suit::hearts, Rank::Ace},
        {Suit::hearts, Rank::r_5},
        {Suit::hearts, Rank::r_4},
        {Suit::hearts, Rank::r_2},
        {Suit::hearts, Rank::r_3},
        {Suit::diamonds, Rank::Ace},
        {Suit::clubs, Rank::r_5}
                );

    BOOST_CHECK(hand.isStraightFlush());
}

BOOST_AUTO_TEST_CASE(TestIsStraightSteelWheelTwoMissing)
{
    hand.setCards(
        {Suit::diamonds, Rank::Ace},
        {Suit::diamonds, Rank::r_5},
        {Suit::diamonds, Rank::r_4},
        {Suit::diamonds, Rank::r_3},
        {Suit::spades, Rank::r_2},
        {Suit::spades, Rank::Ace},
        {Suit::clubs, Rank::r_2}
                );

    BOOST_CHECK(!hand.isStraightFlush());
}

/*
 * FOUR OF A KIND
 */
BOOST_AUTO_TEST_CASE(TestIsFourOfAKind)
{
    hand.setCards(
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::Ace},
        {Suit::diamonds, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::spades, Rank::Queen},
        {Suit::spades, Rank::Ace},
        {Suit::clubs, Rank::Queen}
                );

    BOOST_CHECK(hand.isFourOfAKind());
}

BOOST_AUTO_TEST_CASE(TestIsFourOfAKindNotEnough)
{
    hand.setCards(
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::Ace},
        {Suit::diamonds, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::spades, Rank::Queen},
        {Suit::spades, Rank::Ace},
        {Suit::clubs, Rank::King}
                );

    BOOST_CHECK(!hand.isFourOfAKind());
}

/*
 * FULL HOUSE
 */
BOOST_AUTO_TEST_CASE(TestIsFullHouse)
{
    hand.setCards(
        {Suit::hearts, Rank::King},
        {Suit::hearts, Rank::r_10},
        {Suit::diamonds, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::spades, Rank::Queen},
        {Suit::spades, Rank::Ace},
        {Suit::clubs, Rank::r_10}
                );

    BOOST_CHECK(hand.isFullHouse());
}

BOOST_AUTO_TEST_CASE(TestIsFullHouseTwoTriplets)
{
    hand.setCards(
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::diamonds, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::spades, Rank::Queen},
        {Suit::spades, Rank::Ace},
        {Suit::clubs, Rank::r_10}
                );

    BOOST_CHECK(hand.isFullHouse());
}

/*
 * FLUSH
 */
BOOST_AUTO_TEST_CASE(TestIsFlush)
{
    hand.setCards(
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::diamonds, Rank::Queen},
        {Suit::diamonds, Rank::r_9},
        {Suit::spades, Rank::Queen},
        {Suit::diamonds, Rank::King},
        {Suit::diamonds, Rank::r_8}
                );

    BOOST_CHECK(hand.isFlush());
}

BOOST_AUTO_TEST_CASE(TestIsFlushNotEnough)
{
    hand.setCards(
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_10},
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_9},
        {Suit::spades, Rank::Queen},
        {Suit::diamonds, Rank::King},
        {Suit::diamonds, Rank::r_8}
                );

    BOOST_CHECK(!hand.isFlush());
}

/*
 * STRAIGHT
 */
BOOST_AUTO_TEST_CASE(TestIsStraightHigh)
{
    hand.setCards(
        {Suit::hearts, Rank::King},
        {Suit::diamonds, Rank::Queen},
        {Suit::clubs, Rank::Jack},
        {Suit::spades, Rank::r_10},
        {Suit::spades, Rank::r_6},
        {Suit::diamonds, Rank::r_2},
        {Suit::diamonds, Rank::Ace}
                );

    BOOST_CHECK(hand.isStraight());
}

BOOST_AUTO_TEST_CASE(TestIsStraightLow)
{
    hand.setCards(
        {Suit::hearts, Rank::r_6},
        {Suit::diamonds, Rank::Queen},
        {Suit::clubs, Rank::r_4},
        {Suit::spades, Rank::r_3},
        {Suit::spades, Rank::r_9},
        {Suit::diamonds, Rank::r_2},
        {Suit::diamonds, Rank::r_5}
                );

    BOOST_CHECK(hand.isStraight());
}

/*
 * TWO PAIRS
 */
BOOST_AUTO_TEST_CASE(TestIsTwoPairs)
{
    hand.setCards(
        {Suit::hearts, Rank::r_6},
        {Suit::diamonds, Rank::Queen},
        {Suit::clubs, Rank::r_4},
        {Suit::spades, Rank::r_6},
        {Suit::spades, Rank::r_9},
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_5}
                );

    BOOST_CHECK(hand.isTwoPairs());
}

BOOST_AUTO_TEST_CASE(TestIsTwoPairsJustOne)
{
    hand.setCards(
        {Suit::hearts, Rank::r_6},
        {Suit::diamonds, Rank::Queen},
        {Suit::clubs, Rank::r_4},
        {Suit::spades, Rank::r_7},
        {Suit::spades, Rank::r_9},
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_5}
                );

    BOOST_CHECK(!hand.isTwoPairs());
}

BOOST_AUTO_TEST_CASE(TestIsTwoPairsPlusOne)
{
    hand.setCards(
        {Suit::hearts, Rank::r_6},
        {Suit::diamonds, Rank::Queen},
        {Suit::clubs, Rank::r_4},
        {Suit::spades, Rank::r_6},
        {Suit::spades, Rank::r_9},
        {Suit::hearts, Rank::Queen},
        {Suit::diamonds, Rank::r_4}
                );

    BOOST_CHECK(hand.isTwoPairs());
    BOOST_CHECK(!hand.isFullHouse());
}

BOOST_AUTO_TEST_SUITE_END()
