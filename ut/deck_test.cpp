#include "poker.h"

#include <algorithm>
#include <map>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(deck_test_suite)

BOOST_AUTO_TEST_CASE(TestInit)
{
    Deck deck;

    std::map<Rank, std::size_t> hist_rank;
    std::map<Suit, std::size_t> hist_suit;

    for (auto i = 0u; i < Deck::NUM_CARDS; ++i)
    {
        ++hist_rank[deck[i].rank];
        ++hist_suit[deck[i].suit];
    }

    BOOST_CHECK_EQUAL(13u, hist_rank.size());
    BOOST_CHECK_EQUAL(4u, hist_suit.size());

    BOOST_CHECK(
                std::all_of(
                    std::cbegin(hist_rank), std::cend(hist_rank),
                    [](const auto& p)
                    {
                        return p.second == 4u;
                    }));

    BOOST_CHECK(
                std::all_of(
                    std::cbegin(hist_suit), std::cend(hist_suit),
                    [](const auto& p)
                    {
                        return p.second == 13u;
                    }));
}

BOOST_AUTO_TEST_SUITE_END()
