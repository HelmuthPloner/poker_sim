#include <array>
#include <algorithm>
#include <cstddef>
#include <iosfwd>

enum class Suit
{
    diamonds,
    clubs,
    hearts,
    spades,
};

enum class Rank
{
    Ace,
    King,
    Queen,
    Jack,
    r_10,
    r_9,
    r_8,
    r_7,
    r_6,
    r_5,
    r_4,
    r_3,
    r_2,
};

struct Card
{
    Suit suit;
    Rank rank;
};

class Deck
{
public:
    Deck();

    Card operator[](const std::size_t i) const
    {
        return cards.at(i);
    }

    static constexpr std::size_t NUM_CARDS = 52;

private:
    std::array<Card, NUM_CARDS> cards;
};

using Hist = std::array<std::size_t, 13>;

class Hand
{
public:
    Hand() = default;

    void setCards(Card c1, Card c2, Card c3, Card c4, Card c5, Card c6, Card c7) noexcept
    {
        cards[0] = c1;
        cards[1] = c2;
        cards[2] = c3;
        cards[3] = c4;
        cards[4] = c5;
        cards[5] = c6;
        cards[6] = c7;

        makeRankHist();
        makeSuitHist();
        if (isFlush()) fillSorted();
    }

    Card operator[](const std::size_t i) const noexcept
    {
        return cards[i];
    }

    auto begin() noexcept
    {
        return cards.begin();
    }

    auto begin() const noexcept
    {
        return cards.begin();
    }

    auto cbegin() const noexcept
    {
        return cards.cbegin();
    }

    auto end() noexcept
    {
        return cards.end();
    }

    auto end() const noexcept
    {
        return cards.end();
    }

    auto cend() const noexcept
    {
        return cards.cend();
    }

    Hist getRankHist() const noexcept
    {
        return rank_hist;
    }

    bool isRoyalFlush() const;
    bool isStraightFlush() const;
    bool isFourOfAKind() const;
    bool isFullHouse() const;
    bool isFlush() const;
    bool isStraight() const;
    bool isThreeOfAKind() const;
    bool isTwoPairs() const;
    bool isTwoOfAKind() const;

private:
    void makeRankHist() noexcept
    {
        rank_hist.fill(0u);

        for (const auto card : cards)
        {
            ++rank_hist[static_cast<std::size_t>(card.rank)];
        }
    }

    void makeSuitHist() noexcept
    {
        suit_hist.fill(0u);

        for (const auto card : cards)
        {
            ++suit_hist[static_cast<std::size_t>(card.suit)];
        }
    }

    void fillSorted() noexcept
    {
        sortedSuitRank = cards;

        std::sort(
                    std::begin(sortedSuitRank), std::end(sortedSuitRank),
                    [](const Card& c1, const Card& c2)
        {
            return std::tie(c1.suit, c1.rank) < std::tie(c2.suit, c2.rank);
        });
    }

    std::array<Card, 7u> cards;
    std::array<Card, 7u> sortedSuitRank;
    Hist rank_hist;
    std::array<std::size_t, 4> suit_hist;
};

std::ostream& operator<<(std::ostream& os, Suit s);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Hand h);
