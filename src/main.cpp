#include "poker.h"

#include <chrono>
#include <iomanip>
#include <iostream>

int main()
{
    auto start = std::chrono::high_resolution_clock::now();

    Deck deck;

    Hand hand;

    std::size_t num_royal_flush = 0u;
    std::size_t num_straight_flush = 0u;
    std::size_t num_four_of_a_kind = 0u;
    std::size_t num_full_house = 0u;
    std::size_t num_flush = 0u;
    std::size_t num_straight = 0u;
    std::size_t num_three_of_a_kind = 0u;
    std::size_t num_two_pairs = 0u;
    std::size_t num_two_of_a_kind = 0u;

    for (auto a = 0u; a < Deck::NUM_CARDS - 6u; ++a)
    {
        for (auto b = a + 1u; b < Deck::NUM_CARDS - 5u; ++b)
        {
            for (auto c = b + 1u; c < Deck::NUM_CARDS - 4u; ++c)
            {
                for (auto d = c + 1u; d < Deck::NUM_CARDS - 3u; ++d)
                {
                    for (auto e = d + 1u; e < Deck::NUM_CARDS - 2u; ++e)
                    {
                        for (auto f = e + 1u; f < Deck::NUM_CARDS - 1u; ++f)
                        {
                            for (auto g = f + 1u; g < Deck::NUM_CARDS; ++g)
                            {
                                hand.setCards(
                                            deck[a], deck[b], deck[c], deck[d],
                                            deck[e], deck[f], deck[g]);

                                if (hand.isFlush())
                                {
                                    if (hand.isRoyalFlush())
                                    {
                                        ++num_royal_flush;
                                    }
                                    else if (hand.isStraightFlush())
                                    {
                                        ++num_straight_flush;
                                    }
                                    else
                                    {
                                        ++num_flush;
                                    }
                                }
                                else if (hand.isFourOfAKind())
                                {
                                    ++num_four_of_a_kind;
                                }
                                else if (hand.isFullHouse())
                                {
                                    ++num_full_house;
                                }
                                else if (hand.isStraight())
                                {
                                    ++num_straight;
                                }
                                else if (hand.isThreeOfAKind())
                                {
                                    ++num_three_of_a_kind;
                                }
                                else if (hand.isTwoPairs())
                                {
                                    ++num_two_pairs;
                                }
                                else if (hand.isTwoOfAKind())
                                {
                                    ++num_two_of_a_kind;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    auto stop = std::chrono::high_resolution_clock::now();

    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << "ms" << std::endl;

    std::cout << "Number of Royal Flush:     " << std::setw(10) << std::right << num_royal_flush << std::endl;
    std::cout << "Number of Straight Flush:  " << std::setw(10) << std::right << num_straight_flush << std::endl;
    std::cout << "Number of Four of a Kind:  " << std::setw(10) << std::right << num_four_of_a_kind << std::endl;
    std::cout << "Number of Full House:      " << std::setw(10) << std::right << num_full_house << std::endl;
    std::cout << "Number of Flush:           " << std::setw(10) << std::right << num_flush << std::endl;
    std::cout << "Number of Straight:        " << std::setw(10) << std::right << num_straight << std::endl;
    std::cout << "Number of Three of a Kind: " << std::setw(10) << std::right << num_three_of_a_kind << std::endl;
    std::cout << "Number of Two Pairs:       " << std::setw(10) << std::right << num_two_pairs << std::endl;
    std::cout << "Number of Two of a Kind:   " << std::setw(10) << std::right << num_two_of_a_kind << std::endl;
}
