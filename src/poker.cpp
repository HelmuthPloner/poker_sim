#include "poker.h"

#include <algorithm>
#include <initializer_list>
#include <iterator>
#include <map>
#include <ostream>

std::ostream& operator<<(std::ostream& os, Suit s)
{
    switch (s)
    {
    case Suit::diamonds: os << u8"\u2662"; break;
    case Suit::clubs: os << u8"\u2667"; break;
    case Suit::hearts: os << u8"\u2661"; break;
    case Suit::spades: os << u8"\u2664"; break;
    }

    return os;
}

std::ostream& operator<<(std::ostream& os, Rank r)
{
    switch (r)
    {
    case Rank::Ace: os << 'A'; break;
    case Rank::King: os << 'K'; break;
    case Rank::Queen: os << 'Q'; break;
    case Rank::Jack: os << 'J'; break;
    case Rank::r_10: os << "10"; break;
    case Rank::r_9: os << '9'; break;
    case Rank::r_8: os << '8'; break;
    case Rank::r_7: os << '7'; break;
    case Rank::r_6: os << '6'; break;
    case Rank::r_5: os << '5'; break;
    case Rank::r_4: os << '4'; break;
    case Rank::r_3: os << '3'; break;
    case Rank::r_2: os << '2'; break;
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, Card c)
{
    os << c.rank << ' ' << c.suit;
    return os;
}

std::ostream& operator<<(std::ostream& os, Hand h)
{
    for (const auto& card : h)
    {
        os << card << " - ";
    }
    return os;
}

bool Hand::isRoyalFlush() const
{
    if (!isFlush()) return false;

    for (auto i = 0u; i < 3u; ++i)
    {
        const auto suit = sortedSuitRank[i].suit;

        // check for matching suit
        if (std::all_of(
                    std::cbegin(sortedSuitRank) + i, std::cbegin(sortedSuitRank) + i + 5,
                    [suit](const auto c) noexcept { return c.suit == suit; }))
        {
            if (sortedSuitRank[i].rank == Rank::Ace && sortedSuitRank[i + 4].rank == Rank::r_10)
            {
                return true;
            }
        }
    }

    return false;
}

bool Hand::isStraightFlush() const
{
    if (!isFlush()) return false;

    for (auto i = 0u; i < 3u; ++i)
    {
        const auto suit = sortedSuitRank[i].suit;

        // check for matching suit
        if (std::all_of(
                    std::cbegin(sortedSuitRank) + i, std::cbegin(sortedSuitRank) + i + 5,
                    [suit](const auto c) noexcept { return c.suit == suit; }))
        {
            if (static_cast<int>(sortedSuitRank[i].rank) + 4 == static_cast<int>(sortedSuitRank[i + 4].rank))
            {
                return true;
            }
        }
    }

    // check for steel wheel
    for (auto i = 1u; i < 4u; ++i)
    {
        const auto suit = sortedSuitRank[i].suit;

        if (std::all_of(
                    std::cbegin(sortedSuitRank) + i, std::cbegin(sortedSuitRank) + i + 4,
                    [suit](const auto c) noexcept { return c.suit == suit; }))
        {
            if (sortedSuitRank[i].rank == Rank::r_5 && sortedSuitRank[i + 3].rank == Rank::r_2)
            {
                if (std::find_if(
                            std::cbegin(sortedSuitRank), std::cend(sortedSuitRank),
                            [suit](const auto c) noexcept { return c.rank == Rank::Ace && c.suit == suit; } ) != std::cend(sortedSuitRank))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

bool Hand::isFourOfAKind() const
{
    return std::find(std::cbegin(rank_hist), std::cend(rank_hist), 4) != std::cend(rank_hist);
}

bool Hand::isFullHouse() const
{
    auto triplet_found = false;
    auto duplicate_found = false;

    for (const auto p : rank_hist)
    {
        switch (p)
        {
        case 3:
            // already one triplet found, so count the second one as the duplicates
            if (triplet_found) duplicate_found = true;
            triplet_found = true;
            break;
        case 2:
            duplicate_found = true;
            break;
        }
    }

    return triplet_found && duplicate_found;
}

bool Hand::isFlush() const
{
    return
            std::find_if(
                std::cbegin(suit_hist), std::cend(suit_hist),
                [](const auto h) noexcept { return 5u <= h; }) != std::cend(suit_hist);
}

// fünf aufeinander folgende Karten, nicht unbedingt gleiche Farbe
bool Hand::isStraight() const
{
    auto h = cards;

    std::sort(
            std::begin(h), std::end(h),
            [](const Card c1, const Card c2) noexcept
            {
                return c1.rank < c2.rank;
            });

    auto no_duplicates = decltype(h)();

    const auto start_duplicates =
            std::unique_copy(
                std::cbegin(h), std::cend(h),
                std::begin(no_duplicates),
                [](const auto c1, const auto c2) noexcept { return c1.rank == c2.rank; });

    const auto uniq_size = std::distance(no_duplicates.begin(), start_duplicates);

    if (uniq_size < 5u) return false;

    for (auto i = 0u; i < uniq_size - 4u; ++i)
    {
        if (static_cast<int>(no_duplicates[i].rank) + 4 == static_cast<int>(no_duplicates[i + 4].rank))
        {
            return true;
        }
    }

    return
            no_duplicates[0].rank == Rank::Ace &&
            no_duplicates[uniq_size - 4].rank == Rank::r_5 &&
            no_duplicates[uniq_size - 1].rank == Rank::r_2;
}

bool Hand::isThreeOfAKind() const
{
    return std::find(std::cbegin(rank_hist), std::cend(rank_hist), 3) != std::cend(rank_hist);
}

bool Hand::isTwoOfAKind() const
{
    return std::find(std::cbegin(rank_hist), std::cend(rank_hist), 2) != std::cend(rank_hist);
}

bool Hand::isTwoPairs() const
{
    return std::count(std::cbegin(rank_hist), std::cend(rank_hist), 2u) >= 2u;
}

Deck::Deck()
{
    std::size_t i = 0u;

    for (const auto& suit : { Suit::diamonds, Suit::clubs, Suit::hearts, Suit::spades } )
    {
        for (const auto& rank : { Rank::Ace, Rank::King, Rank::Queen, Rank::Jack, Rank::r_10, Rank::r_9, Rank::r_8, Rank::r_7, Rank::r_6, Rank::r_5, Rank::r_4, Rank::r_3, Rank::r_2 })
        {
            cards[i++] = Card{suit, rank};
        }
    }
}
